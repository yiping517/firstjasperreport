package test;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.base.JRBaseTextField;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class FirstReport {

	public static void main(String[] args) {
		
		try {
			String filePath = "C:\\practice\\JasperReport\\src\\main\\resources\\FirstReport.jrxml";
			//String generatedFilePath = filePath + File.separator + "FirstReport.pdf";
			//跟jrxml同一個目錄會出現error
			
			String generatedFilePath = "C:\\practice\\JasperReport\\src\\main\\resources\\PDF" + File.separator + "FirstReport.pdf";
			
			//set the value for the parameters in jasper report
			//(方式1)
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("studentName", "john");
			//(方式2)
			Student student1 = new Student(1L,"firstname1","lastname1","address1","city1");
			Student student2 = new Student(2L,"firstname2","lastname2","address2","city2");
			Student student3 = new Student(3L,"firstname3","lastname3","address3","city3");
			Student student4 = new Student(4L,"firstname4","lastname4","address4","city4");
			List<Student> list = new ArrayList<Student>();
			list.add(student1);
			list.add(student2);
			list.add(student3);
			list.add(student4);
			
			//pass data source to jasper report
			
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
			
			JasperReport report = JasperCompileManager.compileReport(filePath);
			
			//runtime change
			JRBaseTextField textField = (JRBaseTextField)report.getTitle().getElementByKey("name");
			textField.setForecolor(Color.orange);
			
			
			JasperPrint print = JasperFillManager.fillReport(report, param, dataSource);
			JasperExportManager.exportReportToPdfFile(print, generatedFilePath);
			System.out.println("Report created...");
			
		}catch(Exception e) {
			System.out.println("exception while creating report");
			e.printStackTrace();
		}
	}

}
